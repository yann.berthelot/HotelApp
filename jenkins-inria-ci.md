# Create a project on Inria CI infrastructure
- Create an account on https://ci.inria.fr
- Host a new project named « ci-training-lil-<your initials> »
- Create a new slave for this project
  - Use a « featured » Template (pre-configured template fully integrated with Jenkins)
  - Small instance
  - No need for extra disk
  - Don’t forget to restart Jenkins to benefit from the slave declaration into Jenkins

# Configure a way to access slaves
First, we need to do some configuration to be able to connect to CI servers through the gateway. On your computer, add these lines into the $HOME/.ssh/config file :
```bash
Host=*.ci
ProxyCommand ssh <yourlogin>@ci-ssh.inria.fr "/usr/bin/nc `basename %h .ci` %p"
```
You can find `<yourlogin` from the ci portal: click on your name (on the top-right corner) / My account. `<yourlogin>` is the uid.
It will automatically use the CI Gateway to reach CI servers. For convenience, pay attention to add your SSH public key to your account on https://ci.inria.fr (My account). 

Now, you can connect to the slave:
```sh
$ ssh ci@<slave-ip-address>.ci
```
